Infobeamer Interactive Slides
=============================

This is a collection of application / views that bring self-updating content to
the Hacken Open Air Infobeamer.

Installation
------------

    python3 -m venv venv
    source venv/bin/activate
    pip install -r REQUIREMENTS.txt

Running the Application
-----------------------

    source venv/bin/activate
    python3 main.py

Then access http://localhost:8080/workshops or http://localhost.8080/elfen