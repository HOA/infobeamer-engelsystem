from __future__ import annotations

import requests
from bs4 import BeautifulSoup
import datetime
from dataclasses import dataclass
from typing import List
import os
import re
from icalendar import Calendar, Event, vCalAddress, vText
import pytz
import hashlib

from lona.html import HTML, H2, H3, Div, Br, Span, Ul, Li, Strong
from lona import LonaApp, LonaView


day_re = re.compile(r'(\d\d\.\d\d\.\d\d\d\d)')
time_re = re.compile(r'(\d\d:\d\d)-(\d\d:\d\d)')


@dataclass
class Workshop:
    day: str
    start: datetime.datetime
    end: datetime.datetime
    where: str
    title: str
    contact: str


def get_workshops():
    r = requests.get('https://wiki.hackenopenair.de/public/workshops')
    r.raise_for_status()
    soup = BeautifulSoup(r.text, 'html.parser')
    talks = []

    day = None
    for node in soup.find_all():
        if node.name == 'h2':
            d = node.text.strip()
            match = day_re.findall(d)
            if match:
                day = match[0]
            else:
                day = None
                continue
        if day == None:
            continue
        if node.name == 'table':
            header = True
            for row in node.find_all('tr'):
                if header:
                    header = False
                    continue
                tds = row.find_all('td')
                if len(tds) < 4:
                    continue

                match = time_re.findall(tds[0].text.strip())
                if match:
                    start = datetime.datetime.strptime(f'{day} {match[0][0]}', '%d.%m.%Y %H:%M')
                    end = datetime.datetime.strptime(f'{day} {match[0][1]}', '%d.%m.%Y %H:%M')
                ws = Workshop(
                    day=day,
                    start=start,
                    end=end,
                    where=tds[1].text.strip(),
                    title=tds[2].text.strip(),
                    contact=tds[3].text.strip(),
                )
                talks.append(ws)
    return sorted(talks, key=lambda x: x.start)


class EngelCache:

    _self = None

    def __init__(self, base_url, username, password):
        print("Engel Cache created")
        self._base_url = base_url
        self._username = username
        self._password = password

        self._ts = None
        self._cache = None

    @staticmethod
    def engel_cache_factory(base_url, username, password):
        if not EngelCache._self:
            EngelCache._self = EngelCache(base_url, username, password)
        return EngelCache._self

    def get_cached(self):
        if not self._cache:
            return self.get_shifts_24h()
        return self._cache

    def update_cache(self):
        print("Getting new data")
        es = Engelsystem(self._base_url, self._username, self._password)
        self._cache = es.get_shifts_24h()
        now = datetime.datetime.now()
        self._ts = now

    def get_shifts_24h(self):
        now = datetime.datetime.now()
        if self._ts and self._ts + datetime.timedelta(minutes=1) > now:
            print("using cached data")
        else:
            self.update_cache()
        return self._cache



class Engelsystem:

    URL_LOGIN = '/login'
    USER_SHIFTS = '/user_shifts'
    SHIFT = '/shifts'
    PUBLIC_DASHBOARD = '/public_dashboard'

    def __init__(self, base_url, username, password):
        self._base_url = base_url

        self.rooms = {}
        self.angeltypes = {}

        self._session = requests.session()
        self._login(username, password)
        self._get_base_data()

    def _login(self, username, password):
        r = self._session.get(f'{self._base_url}{Engelsystem.URL_LOGIN}')
        r.raise_for_status()

        soup = BeautifulSoup(r.text, 'html.parser')
        token = soup.find('input', attrs={'name': '_token'}).attrs['value']
        if not token:
            raise Exception('Could not find token')

        r = self._session.post(
            f'{self._base_url}{Engelsystem.URL_LOGIN}',
            data={
                '_token': token,
                'login': username,
                'password': password,
            }
        )
        r.raise_for_status()

    def _starttime_shift(self, shift_url):
        shift_id = shift_url.split('=')[-1]
        rs = self._session.get(
            f'{self._base_url}{Engelsystem.SHIFT}',
            params={
                 'action': 'view',
                 'shift_id': shift_id,
            },
        )
        rs.raise_for_status()
        ssoup = BeautifulSoup(rs.text, 'html.parser')

        start = ''
        for div in ssoup.find_all(class_='col-sm-3 col-xs-6'):
            if 'Start' in div.text:
                return ' '.join([x.strip() for x in div.find('p').text.split('\n') if x.strip()])

    def _get_base_data(self):
        r = self._session.get(f'{self._base_url}{Engelsystem.USER_SHIFTS}')
        r.raise_for_status()

        soup = BeautifulSoup(r.text, 'html.parser')

        # find rooms
        for inp in soup.find_all('input', attrs={'name': 'rooms[]'}):
            label = inp.findParent()
            room_id = inp.attrs['value']
            room = label.text.strip()
            self.rooms[room_id] = room
            print(f'Discovered room {room}')

        # find angeltypes
        for inp in soup.find_all('input', attrs={'name': 'types[]'}):
            label = inp.findParent()
            type_id = inp.attrs['value']
            type_ = label.text.strip()
            self.angeltypes[type_id] = type_
            print(f'Discovered angeltype {type_}')

    @dataclass
    class Shift:
        shifttype: str
        shift_url: str
        infotext: str
        location: str
        angels_needed_total: int
        time: str

    def get_shifts_24h(self) -> List[Engelsystem.Shift]:
        shifts = []

        r = self._session.get(
            f"{self._base_url}{Engelsystem.PUBLIC_DASHBOARD}",
        )
        r.raise_for_status()
        soup = BeautifulSoup(r.text, "html.parser")
        for shift in soup.find_all(class_='border-danger'):
            anchors = shift.find_all('a')

            shift_url = anchors[0].attrs['href']
            strs = [s for s in shift.strings]
            sp = strs[3].rsplit('(', 1)
            shifttype = sp[0].strip()
            shift_id = ''
            if len(sp) > 1:
                shift_id = sp[1].rstrip(')').strip()
            location = strs[5].strip()

            required_elfen = 0
            for i in range(8, len(strs), 3):
                required_elfen += int(strs[i].split('×')[0])

            shifts.append(Engelsystem.Shift(
                shift_url=shift_url,
                shifttype=shifttype,
                location=location,
                infotext=shift_id,
                angels_needed_total=required_elfen,
                time=self._starttime_shift(shift_url),
            ))

        now = datetime.datetime.now()
        tomorrow = now + datetime.timedelta(days=1)
        r = self._session.get(
            f'{self._base_url}{Engelsystem.USER_SHIFTS}',
            params={
                'p': 'user_shifts',
                'start_day': now.strftime('%Y-%m-%d'),
                'start_time': now.strftime('%H:%M'),
                'end_day': tomorrow.strftime('%Y-%m-%d'),
                'end_time': tomorrow.strftime('%H:%M'),
                'rooms[]': [k for k, _ in self.rooms.items()],
                'types[]': [k for k, _ in self.angeltypes.items()],
                'filled[]': ['0', '1'],
            },
        )
        r.raise_for_status()
        soup = BeautifulSoup(r.text, 'html.parser')


        for shift in soup.find_all(class_='shift'):
            anchors = shift.find_all('a')

            shift_url = anchors[0].attrs['href']
            shifttype=anchors[0].find(text=True, recursive=False).split('—')[1].strip(),
            infotext=shift.find(class_='card-body').find_all(text=True)[1].strip(),

            angels_needed_total = 0
            try:
                angels_needed_total = int(shift.find_all('a')[0].find('span').text.strip())
            except AttributeError:
                pass


            s = Engelsystem.Shift(
                shift_url=shift_url,
                shifttype=anchors[0].find(text=True, recursive=False).split('—')[1].strip(),
                location=shift.find(class_='card-body').find_all(text=True)[-1].strip(),
                infotext=shift.find(class_='card-body').find_all(text=True)[1].strip(),
                angels_needed_total=angels_needed_total,
                time=self._starttime_shift(shift_url),
            )
            shifts.append(s)

        return sorted(shifts, key=lambda x: x.time)


app = LonaApp(__file__)


@app.route('/ics', interactive=False)
class IcsView(LonaView):
    def handle_request(self, request):
        tz = pytz.timezone('Europe/Berlin')

        cal = Calendar()
        cal.add('prodid', 'HOA Workshop ical export')
        cal.add('version', '0.1')

        for workshop in get_workshops():
            event = Event()

            lines = workshop.title.split('\n')
            event.add('summary', lines[0])

            if len(lines) > 1:
                event.add('description', '\n'.join(lines[1:]))

            event.add('dtstart', tz.localize(workshop.start))
            event.add('dtend', tz.localize(workshop.end))
            event['location'] = vText(workshop.where)
            event['uid'] = hashlib.sha256(str(workshop).encode()).hexdigest()

            person = vCalAddress('')
            person.params['name'] = vText(workshop.contact)
            event['organizer'] = person

            cal.add_component(event)
        return {
            'content_type': 'text/calendar',
            'headers': {
                'Content-Disposition': f'attachment; filename="hoa.ics"',
            },
            'body': cal.to_ical(),
        }


@app.route('/workshops')
class WorkshopsView(LonaView):
    def handle_request(self, request):
        html = HTML(
            H3('Lade...'),
        )
        self.show(html)

        html = HTML(
            H2("Kommende Workshops und Talks"),
            workshops := Ul(),
        )

        now = datetime.datetime.now()
        for ws in get_workshops():
            if ws.end < now:
                continue
            workshops.nodes.append(
                Li(
                    f'{ws.start.strftime("%a %d.%m %H:%M")} - {ws.end.strftime("%H:%M")} ',
                    Strong(ws.title),
                    Br(),
                    f'By: {ws.contact}',
                    Br(),
                    f'@ {ws.where}',
                    style='margin-top: 20px; font-size: xx-large;'
                )
            )
        return html


@app.route('/elfen')
class ElfenView(LonaView):
    def handle_request(self, request):
        html = HTML(
            H3('Lade...'),
        )
        self.show(html)

        html = HTML(
            H2('Benötigte Helfer in den nächsten 24 Stunden'),
            Div('Schichten in denen Engel fehlen: ', shifts_missing_help := Span(), style='font-size: xx-large;'),
            Div('Fehlende Elfen: ', missing_elves := Span(), style='font-size: xx-large;'),

            H2('Offene Schichten'),
            schichten := Ul(),
        )

        es = EngelCache.engel_cache_factory('https://elfen.hackenopenair.de', '_infobeamer', '_infobeamer')
        shifts = es.get_cached()
        print(shifts)
        print([s.angels_needed_total for s in shifts])

        shifts_missing_help.set_text(str(len([s for s in shifts if s.angels_needed_total > 0])))
        missing_elves.set_text(str(sum([s.angels_needed_total for s in shifts])))

        for shift in shifts:
            if shift.angels_needed_total > 0:
                schichten.nodes.append(
                    Li(f'{shift.time} {shift.shifttype} {shift.infotext} @ {shift.location}', style='font-size: xx-large; margin-top: 12px;')
                )
        self.show(html=html)
        es.update_cache()
        return html

basepath = os.path.dirname(os.path.realpath(__file__))
app.settings.FRONTEND_TEMPLATE = '/theme/templates/base.html'
app.settings.TEMPLATE_DIRS.append(basepath)
app.settings.STATIC_DIRS = [os.path.join(basepath, 'theme', 'static')]
app.settings.CORE_MIDDLEWARES = ['lona.middlewares.lona_messages.LonaMessageMiddleware']
app.run(host='0.0.0.0')

# vim: expandtab:shiftwidth=4:softtabstop=4
